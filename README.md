# JIXSERVICES
Bienvenidos a nuestro proyecto de final de CFGS ASIXc.

Integrantes:
- Isaac Cerezo
- Joel Duran
- Xavier Siles

### Objetivo del proyecto:
El principal objetivo de nuestro trabajo es crear una plataforma gratuita donde nosotros y
nuestros amigos podamos almacenar datos, colaborar en proyectos, crear calendarios...
todo esto lo haremos con el servicio NextCloud. También tenemos pensado ofrecer un
servicio de VPN.

El principal objetivo de nuestro trabajo es crear con pocos recursos un servidor donde
ofreceremos servicios de almacenamiento en la nube y VPN.
